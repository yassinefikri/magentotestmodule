<?php

declare(strict_types=1);

namespace Formation\Test\Setup\Patch\Schema;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResource;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Zend_Validate_Exception;

class AddLinkedInProfileAttributeToDatabase implements DataPatchInterface, PatchRevertableInterface
{
    private const LINKEDIN_FIELD_ATTRIBUTE_NAME = 'linkedin_profile';
    private const LINKEDIN_FIELD_ATTRIBUTE_LABEL = 'LinkedIn Profile';

    private ModuleDataSetupInterface $moduleDataSetup;

    private CustomerSetup $customerSetup;

    private AttributeSetFactory $attributeSetFactory;

    private AttributeResource $attributeResource;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        AttributeResource $attributeResource
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetup = $customerSetupFactory->create(['setup' => $moduleDataSetup]);
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeResource = $attributeResource;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function getAliases(): array
    {
        return [];
    }

    /**
     * @throws Zend_Validate_Exception
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    public function apply(): void
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->customerSetup->addAttribute(Customer::ENTITY, self::LINKEDIN_FIELD_ATTRIBUTE_NAME, [
            'backend' => 'Formation\Test\Model\Attribute\Backend\LinkedinProfileAttribute',
            'frontend_class' => 'validate-linkedin_url',
            'type' => 'varchar',
            'label' => self::LINKEDIN_FIELD_ATTRIBUTE_LABEL,
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'system' => 0,
        ]);

        $customerEntity = $this->customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $attribute = $this->customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, self::LINKEDIN_FIELD_ATTRIBUTE_NAME)
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => [
                    'adminhtml_customer',
                    'customer_account_create',
                    'customer_account_edit'
                ],
            ]);

        $this->attributeResource->save($attribute);
    }

    public function revert(): void
    {
        $this->customerSetup->removeAttribute(Customer::ENTITY, self::LINKEDIN_FIELD_ATTRIBUTE_NAME);
    }
}
