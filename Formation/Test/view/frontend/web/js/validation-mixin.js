define(['jquery'], function($) {
    'use strict';

    return function() {
        $.validator.addMethod(
            'validate-linkedin_url',
            function(value, element) {
                return 0 === value.length || (value.length < 250 && /^https:\/\/(www\.)?linkedin\.com/.test(value));
            },
            $.mage.__('Please enter a valid linkedin url')
        )
    }
});
