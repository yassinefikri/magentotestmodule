<?php

declare(strict_types=1);

namespace Formation\Test\Model\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class LinkedinProfileAttribute extends AbstractBackend
{
    private const LINKEDIN_URL_REGEX = '/^https:\/\/(www\.)?linkedin\.com/';
    private const MAX_LENGTH = 250;

    public function beforeSave($object): self
    {
        $this->validateValue($object);

        return parent::beforeSave($object);
    }

    private function validateValue(DataObject $object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $value = $object->getData($attributeCode);

        if (null !== $value &&
            (strlen($value) > self::MAX_LENGTH || 1 !== preg_match(self::LINKEDIN_URL_REGEX, $value))) {
            throw new LocalizedException(__('The Linkedin Profile is not valid'));
        }
    }
}
